package main;

public class Coordinate {
    private char x;
    private int y;

    public Coordinate() {
    }

    ;

    public Coordinate(char x, int y) {
        this.x = x;
        this.y = y;
    }

    public Coordinate(Coordinate coordinate) {
        this.x = coordinate.x;
        this.y = coordinate.y;
    }

    public char getX() {
        return x;
    }

    public void setX(char x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public void set(char x, int y) {
        this.x = x;
        this.y = y;
    }

    public void set(Coordinate coordinate) {
        this.x = coordinate.x;
        this.y = coordinate.y;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Coordinate that = (Coordinate) o;

        if (x != that.x) return false;
        return y == that.y;
    }

    @Override
    public int hashCode() {
        int result = (int) x;
        result = 31 * result + y;
        return result;
    }

    public Coordinate nextUp(int steps) {
        int y=this.getY()-steps;
        if (y>0) return new Coordinate(this.x,y);
        return new Coordinate('W',2);

        }





    public Coordinate nextDown(int steps) {
        int y=this.getY()+steps;
        if (y<11) return new Coordinate(this.x,y);
        return new Coordinate('W',2);

    }



    public Coordinate nextLeft(int steps) {
        int pos = "RESPUBLIKA".indexOf(this.x) - steps;
        if (pos >= 0) return new Coordinate("RESPUBLIKA".charAt(pos), this.y);
        return new Coordinate('W', 2);
    }



    public Coordinate nextRight(int steps) {
        int pos = "RESPUBLIKA".indexOf(this.x) + steps;
        if (pos <10) return new Coordinate("RESPUBLIKA".charAt(pos), this.y);
        return new Coordinate('W', 2);
    }

    @Override
    public String toString() {
        return "" + x + y;
    }


}
