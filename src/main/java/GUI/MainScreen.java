package GUI;

import main.BattlePane;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.MatteBorder;
import java.awt.*;


public class MainScreen {

    public MainScreen(BattlePane t1, BattlePane t2) {

//        new Runnable() {
//            @Override
//            public void run() {
//        EventQueue.invokeLater(() -> {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ex) {
        }

        JFrame frame = new JFrame("ShipBattle");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(new BorderLayout());


        t1.setBorder(new MatteBorder(30, 30, 30, 30, Color.white));

        t2.setBorder(new MatteBorder(30, 30, 30, 30, Color.white));


        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));

        panel.add(t1);
        panel.add(t2);


        frame.add(panel);


        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }
}