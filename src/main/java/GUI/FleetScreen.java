package GUI;

import main.BattlePane;
import main.CellPane;
import main.Coordinate;
import main.Ship;

import javax.swing.*;
import javax.swing.border.MatteBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Map;

import static main.Game.*;

public class FleetScreen {

    private int[] notAssignedShips = {0, 4, 3, 2, 1};
    private ArrayList<Coordinate> blocked = new ArrayList<Coordinate>();
    private JLabel [] leftShipsPpanel= new JLabel[4];
    private JFrame frame = new JFrame("Set up my Fleet");




    public FleetScreen() {

        blocked.add(new Coordinate('W',2));


        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ex) {
        }


        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(new BorderLayout());


        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

        for (int i = 1; i <=4 ; i++) {
            leftShipsPpanel[i-1] = new JLabel();
            leftShipsPpanel[i-1].setText("" + notAssignedShips[i] + " of "+ i+" panel ships left");
            panel.add(leftShipsPpanel[i-1]);
        }



        myFleet.setBorder(new MatteBorder(30, 30, 30, 30, myFleet.getBackground()));

        panel.add(myFleet);

//        JButton done = new JButton("done");
//        done.addActionListener(new ActionListener() {
//            @Override
//            public void actionPerformed(ActionEvent e) {
//                frame.dispose();
//            }
//        });
//
//
//        panel.add(done);

        frame.add(panel);


        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);

//        setUpFleet();


    }

    private int shipsLeft() {
        int s = 0;
        for (int i : notAssignedShips
                ) {
            s = s + i;
        }
        return s;
    }

    public boolean setUpFleet() {
        while (shipsLeft() != 0) {
            for (Map.Entry<Coordinate, CellPane> cell : myFleet.map.entrySet()) {
                if (!blocked.contains(cell.getKey())) cell.getValue().setActive(true);
            }  // set active cells which are not blocked



            while (eventAt.equals(noEvent)) {
                Thread sleep = new Thread();
                try {
                    sleep.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }

            Coordinate first = new Coordinate(eventAt);
            eventAt.set(noEvent);

            for (Map.Entry<Coordinate, CellPane> cell : myFleet.map.entrySet()) {
                cell.getValue().setActive(false);
            } // set all cells blocked

            for (int i=1; i<5; i++){
                if(notAssignedShips[i]!=0){
                    Coordinate testCoordinate = first.nextRight(i-1);
                    if (!blocked.contains(testCoordinate)) myFleet.map.get(testCoordinate).setActive(true);
                    testCoordinate = first.nextLeft(i-1);
                    if (!blocked.contains(testCoordinate)) myFleet.map.get(testCoordinate).setActive(true);
                    testCoordinate = first.nextUp(i-1);
                    if (!blocked.contains(testCoordinate)) myFleet.map.get(testCoordinate).setActive(true);
                    testCoordinate = first.nextDown(i-1);
                    if (!blocked.contains(testCoordinate)) myFleet.map.get(testCoordinate).setActive(true);


                }
            }

            while(eventAt.equals(noEvent)){
                Thread blink = new Thread();
                myFleet.map.get(first).setBackground(Color.BLUE);
                try {
                    blink.sleep(200);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                myFleet.map.get(first).setBackground(Color.WHITE);
                try {
                    blink.sleep(200);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }

            for (Map.Entry<Coordinate, CellPane> cell : myFleet.map.entrySet()) {
                cell.getValue().setActive(false);
            } // set all cells blocked




            Coordinate second = new Coordinate(eventAt);
            eventAt.set(noEvent);
            Ship ship=new Ship(first,second);


            myFleet.addShip(ship);

            System.out.println("Coordinates of the ship" +first+", "+ second);

            for (Coordinate c: ship.getPositions()) {
                if (!blocked.contains(c)) {
                    blocked.add(c);
                    System.out.println("new blocked coordinate" + c);
                }

                if (!blocked.contains(c.nextDown(1))) {
                    blocked.add(c.nextDown(1));
                    System.out.println("new blocked coordinate" + c);
                }

                    if (!blocked.contains(c.nextUp(1))){
                    blocked.add(c.nextUp(1));
                        System.out.println("new blocked coordinate" + c);

                    }
                if (!blocked.contains(c.nextLeft(1))){
                        blocked.add(c.nextLeft(1));
                    System.out.println("new blocked coordinate" + c);

                }
                if (!blocked.contains(c.nextRight(1))){
                    blocked.add(c.nextRight(1));
                    System.out.println("new blocked coordinate" + c);

                }

            }

            Coordinate c = first.nextDown(1).nextLeft(1);
            if(!blocked.contains(c)) blocked.add(c);

            c = first.nextDown(1).nextRight(1);
            if(!blocked.contains(c)) blocked.add(c);

            c = first.nextUp(1).nextLeft(1);
            if(!blocked.contains(c)) blocked.add(c);

            c = first.nextUp(1).nextRight(1);
            if(!blocked.contains(c)) blocked.add(c);

            c = second.nextDown(1).nextLeft(1);
            if(!blocked.contains(c)) blocked.add(c);

            c = second.nextDown(1).nextRight(1);
            if(!blocked.contains(c)) blocked.add(c);

            c = second.nextUp(1).nextLeft(1);
            if(!blocked.contains(c)) blocked.add(c);

            c = second.nextUp(1).nextRight(1);
            if(!blocked.contains(c)) blocked.add(c);


            myFleet.draw(ship);
            notAssignedShips[ship.getLength()]--;
            leftShipsPpanel[ship.getLength()-1].setText("" + notAssignedShips[ship.getLength()] + " of "+ ship.getLength()+" panel ships left");





        }

        frame.dispose();
        return true;


    }
//
//    public class DoneButton extends JButton {
//
//        @Override
//        public Dimension getPreferredSize() {
//            return new Dimension(200, 50);
//        }
//
//
//    }

}




