package main;

import GUI.FleetScreen;
import GUI.MainScreen;
import skype.Skype222;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.Map;

public class Game {

    public static Coordinate eventAt=new Coordinate('W',0);
    public static final Coordinate noEvent = new Coordinate('W',0);
    public static BattlePane myFleet;
    public static BattlePane opponentSea;
    private static boolean myTurn;
    public static String newMessage="";
    private Skype222 skype;
    public static Game game;
    private boolean playing=false;
    private ArrayList<Coordinate> blockedCells =new ArrayList<>();
    private String opponent;



    public Game() {
    }

    public static void main(String[] args) {


        String username = "bela12";
        String password = "bebocka12";
        String opponent = "misha.ha";
        game=new Game();
        game.opponent=opponent;
        game.setUpSkypeConnection(username,password);




        myFleet=new BattlePane();

        opponentSea=new BattlePane();

        FleetScreen setFleet=new FleetScreen();
        Boolean fleetIsSet=setFleet.setUpFleet();

        if(fleetIsSet) {
            MainScreen screen = new MainScreen(opponentSea, myFleet);
        }

        if(newMessage.equals("#ready?")){
            myTurn=false;
            game.sendMessage("#yes");
//            game.play();
//            for (Map.Entry<Coordinate, CellPane> cell : opponentSea.map.entrySet()) {
//                cell.getValue().setActive(true);
//            } // set all cells active;
        }
        else {
            game.sendMessage("#ready?");
        }






// Connect to Skype


//        Skype222 skype= new Skype222(username,password);
//        skype.connectToSkype();
//        System.out.println("connected");


//        skype.enableListener(opponent);
//
//        if (newMessage.equals("#ready?")) {
//            myTurn=false;
//            skype.sendMessage(opponent,"#yes");
//        }else{
//            skype.sendMessage(opponent,"#ready?" );
//        }
//
//
//        if (newMessage.equals("#yes"){
//
//        }



//        skype.sendMessage(opponent, "Hello");


//        int myRandom = (int)(Math.random()*1000000);
//        skype.sendMessage(opponent,"#rand"+myRandom);
//
//        while(!newMessage.contains("#rand")){
//            Thread sleep = new Thread();
//            try {
//                sleep.sleep(100);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//        }

//        int opponentRandom=Integer.parseInt(newMessage.substring(5));
//        System.out.println("random "+ opponentRandom);
//
//        myTurn=(myRandom>opponentRandom)?true:false;
//        myTurn=true;




//        for (Map.Entry<Coordinate, CellPane> cell : opponentSea.map.entrySet()) {
//            cell.getValue().setActive(true);
//        } // set all cells active;
//
//        while(eventAt.equals(noEvent)){
//            Thread sleep = new Thread();
//            try {
//                sleep.sleep(100);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//        }
//
//        skype.sendMessage(opponent,"#"+eventAt);
















    }



    public void setUpSkypeConnection(String username, String password){
        skype= new Skype222(username, password);
        skype.connectToSkype();
        System.out.println("connected");
        listenerEnable();
    }

    public void listenerEnable(){
        skype.enableListener(opponent);

    }

    public void sendMessage(String message){
        skype.sendMessage(opponent,message);
    }

    public void evaluate (String message){
        if (message.equals("#ready?")){
            newMessage=message;
            return;
        }
        if (message.equals("#yes")){
            myTurn=true;
            play();
        }

        else{
            if (!myTurn) {
                Coordinate coordinate = new Coordinate(message.charAt(1), Integer.valueOf(message.substring(2)));
                check(coordinate);
                System.out.println(coordinate);
            }
            else{
                blockedCells.add(new Coordinate(eventAt));
                System.out.println("Shoot number of blocked cells" + blockedCells.size() );
                if(message.equals("#statusNONE")){
                    opponentSea.map.get(eventAt).add(new JLabel("*"));
                    opponentSea.map.get(eventAt).setBackground(Color.green);

                    myTurn=false;
                    eventAt.set(noEvent);
                    for (Map.Entry<Coordinate, CellPane> cell : opponentSea.map.entrySet()) {
                         cell.getValue().setActive(false);
                    } // set all cells false;


                }
                else if(message.equals("#statusINJURED")){
                    System.out.println("Opponent ship is injured!");
                    opponentSea.map.get(eventAt).add(new JLabel("X"));
                    opponentSea.map.get(eventAt).setBackground(Color.orange);
                    for (Map.Entry<Coordinate, CellPane> cell : opponentSea.map.entrySet()){
                        cell.getValue().setActive(false);}
                    System.out.println(opponentSea.map.get(new Coordinate('R',8)).isActive());




                    eventAt.set(noEvent);

                    play();
                }
                else if (message.equals("#statusDEAD"))
                {
                    opponentSea.map.get(eventAt).add(new JLabel("X"));
                    opponentSea.map.get(eventAt).setBackground(Color.red);
                    System.out.println("Opponent ship is DEAD!!!");
                    for (Map.Entry<Coordinate, CellPane> cell : opponentSea.map.entrySet())
                        cell.getValue().setActive(false);

                    eventAt.set(noEvent);
                    play();
                }
            }
        }






    }

    private void check(Coordinate coordinate) {
        Ship ship=myFleet.map.get(coordinate).getShip();
        if (ship==null){
            myFleet.map.get(coordinate).setBackground(Color.green);
            sendMessage("#statusNONE");
            myTurn=true;
            play();
        }
        else{

            myFleet.map.get(coordinate).setHit(true);
            ship.hit();
            if (ship.isDestroyed()) {
                for (Coordinate c : ship.getPositions()){
                    myFleet.map.get(c).setBackground(Color.red);
                }
                sendMessage("#statusDEAD");
            }
            else
                {
                myFleet.map.get(coordinate).setBackground(Color.orange);
                sendMessage("#statusINJURED");
            }


            }


    }

    public void play(){
        System.out.println("Play");
        int activeCells=0;
        System.out.println("Play number of blocked cells " +blockedCells.size() );
        for (Map.Entry<Coordinate, CellPane> cell : opponentSea.map.entrySet()) {
            if (!blockedCells.contains(cell.getKey())) {
                cell.getValue().setActive(true);
                activeCells++;
            }
        } // set all cells active;

        System.out.println("number of Active cells " + activeCells);


        while(eventAt.equals(noEvent)){
            {
                Thread sleep = new Thread();
                try {
                    sleep.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }

        }

        sendMessage("#"+eventAt);


    }

}
