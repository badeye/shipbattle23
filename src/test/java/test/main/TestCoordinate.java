package test.main;

import main.Coordinate;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

public class TestCoordinate {

    @Test
    public void compareCoordinates() {
        Coordinate c1 = new Coordinate('A', 1);
        Coordinate c2 = new Coordinate('B', 2);
        Map<Coordinate, Integer> test= new HashMap<>();
        test.put(c1,70);
        test.put(c2, 20);
        System.out.println(test.get(c2));
        c2.set(c1);
        System.out.println(test.get(c2));

        System.out.println("Can the map be tested for null key " + test.containsKey(null));

    }
}
