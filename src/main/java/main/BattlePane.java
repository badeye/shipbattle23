package main;

import javax.swing.*;
import javax.swing.border.BevelBorder;
import javax.swing.border.Border;
import javax.swing.border.MatteBorder;
import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static java.lang.Math.abs;

public class BattlePane extends JPanel {
    public Map<Coordinate, CellPane> map = new HashMap<>();
    public ArrayList<Ship> KnownShips=new ArrayList<>();


    public BattlePane() {
        setLayout(new GridBagLayout());

        GridBagConstraints gbc = new GridBagConstraints();

        for (int row = 0; row < 11; row++) {
            gbc.gridx = 0;
            gbc.gridy = row;
            LetterPane letterPane = new LetterPane();

            Border border = null;

            if (row < 10) {
                border = new MatteBorder(1, 1, 0, 0, Color.GRAY);
            } else {
                border = new MatteBorder(1, 1, 1, 0, Color.GRAY);
            }
            letterPane.setBorder(border);
            add(letterPane, gbc);
            if (row > 0) letterPane.add(new JLabel("" + row));


        }

        for (int col = 1; col < 11; col++) {
            gbc.gridx = col;
            gbc.gridy = 0;
            LetterPane letterpanel = new LetterPane();
            Border border = null;

            if (col < 10) {
                border = new MatteBorder(1, 1, 0, 0, Color.GRAY);
            } else {
                border = new MatteBorder(1, 1, 0, 1, Color.GRAY);
            }
            letterpanel.setBorder(border);
            add(letterpanel, gbc);
            if (col > 0) letterpanel.add(new JLabel("RESPUBLIKA".substring(col - 1, col)));

        }


        for (int row = 1; row < 11; row++) {
            for (int col = 1; col < 11; col++) {
                gbc.gridx = col;
                gbc.gridy = row;

                CellPane cellPane = new CellPane();
                Coordinate coordinate = new Coordinate("RESPUBLIKA".charAt(col - 1), row);
                cellPane.setCoordinate (coordinate);
                map.put(coordinate, cellPane);
                Border border = null;
                if (row < 10) {
                    if (col < 10) {
                        border = new MatteBorder(1, 1, 0, 0, Color.GRAY);
                    } else {
                        border = new MatteBorder(1, 1, 0, 1, Color.GRAY);
                    }
                } else {
                    if (col < 10) {
                        border = new MatteBorder(1, 1, 1, 0, Color.GRAY);
                    } else {
                        border = new MatteBorder(1, 1, 1, 1, Color.GRAY);
                    }
                }
                cellPane.setBorder(border);
                add(cellPane, gbc);


                cellPane.setActive(false);


            }
        }

    }

    public int canPlaceAShip(Coordinate nose, Coordinate tail) {

        int distance = 5;


        if (nose.getX() == tail.getX()) {
            distance = nose.getY() - tail.getY();
        } else if (nose.getY() == tail.getY()) {
            distance = "RESPUBLIKA".indexOf(nose.getX()) - "RESPUBLIKA".indexOf(tail.getX());
        }

        distance = abs(distance);
        if (distance <= 3) return distance + 1;
        else return 0;
    }


    public void draw(Ship ship) {
        for (Coordinate c: ship.getPositions()){
        this.map.get(c).setBackground(Color.GRAY);
        this.map.get(c).setBorder((new BevelBorder(BevelBorder.RAISED)));
        }

    }

    public void addShip(Ship ship){
        KnownShips.add(ship);
        for (Coordinate cell: ship.getPositions()){
            map.get(cell).setShip(ship);
        }

    }
}
