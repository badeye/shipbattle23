package main;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class CellPane extends JPanel {

    private Color defaultBackground;
    private boolean active = true;
    private Coordinate coordinate;
    private Ship ship=null;
    private boolean hit=false;


    public CellPane() {
        addMouseListener(new MouseAdapter() {
            @Override
            public void mouseEntered(MouseEvent e) {
                if (active) {
                    defaultBackground = getBackground();
                    setBackground(Color.BLUE);
                }
            }

            @Override
            public void mouseExited(MouseEvent e) {
                if (active)
                    setBackground(defaultBackground);
            }

            @Override
            public void mousePressed(MouseEvent e) {
                if (active) {
                    System.out.println("Mouse button pressed");
                    System.out.println("Event at " + coordinate);

                    Game.eventAt.set(coordinate);

                } else
                    System.out.println("Mouse button not pressed");
                ;
            }


        });
    }

    public Coordinate getKoordinate() {
        return coordinate;
    }

    public void setCoordinate(Coordinate coordinate) {
        this.coordinate = coordinate;
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(30, 30);
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Ship getShip() {
        return ship;
    }

    public void setShip(Ship ship) {
        this.ship = ship;
    }

    public boolean isHit() {
        return hit;
    }

    public void setHit(boolean hit) {
        this.hit = hit;
    }
}
