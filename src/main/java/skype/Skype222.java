package skype;

import fr.delthas.skype.Skype;
import fr.delthas.skype.User;
import main.Game;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import static main.Game.game;

public class Skype222 {


    Skype skype;

    public Skype222(String username, String password){

    skype= new Skype(username,password);
    }




//    public static void main(String[] args) {
//
//        String opponent = "misha.ha";
//
//        Skype222 s = new Skype222();
//        s.connectToSkype();
//        s.enableListener(opponent);
//
//        boolean exit=false;
//
//        while(!exit){
//            System.out.println("type here:");
//            Scanner scanner=new Scanner(System.in);
//            String input =scanner.nextLine();
//
//            if (input.toLowerCase().equals("exit")) {
//                exit=true;
//            }
//            else {
//                s.sendMessage(opponent, input);
//            }
//        }
//        s.disconnect();
//
//
//
//    }

    public void connectToSkype() {
        try {
            // If you want to report a bug, enable logging
            // Skype.setDebug(path);
            skype.connect();                         // Will block until we're connected
            System.out.println("Connection succeed");
        } catch (IOException e) {
            System.err.println("An error occured while connecting...");
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void disconnect() {
        skype.disconnect();
        System.out.println("disconnected");
    }

    public void sendMessage(String userName, String message) {
        for (User user : skype.getContacts()) {
            if (user.getUsername().equals(userName)) {
                System.out.println("Message sent: " + message);
                user.sendMessage(message);
            }
        }

    }

    public void enableListener(String userName){
        skype.addUserMessageListener((user, message) -> {
            if (user.getUsername().equals(userName)) {
                if (message.charAt(0) =='#') {
                    System.out.println(message);
                    game.evaluate(message);
                }
            }
        });
    }

    private void evaluateMessage(String message) {

    }



}
