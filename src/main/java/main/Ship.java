package main;

import java.util.ArrayList;

import static java.lang.Math.abs;

public class Ship {
    private int length;
    private ArrayList<Coordinate> positions;
    private SeaOrientation orientation;
    private int hitnumber;
    private boolean destroyed;

    public Ship(Coordinate first, Coordinate second){
        positions=new ArrayList<>();
        hitnumber=0;
        destroyed=false;


        if (second.getX()==first.getX()) orientation=SeaOrientation.VERTICAL;
        else orientation=SeaOrientation.HORIZONTAL;

        if (orientation==SeaOrientation.VERTICAL){
            length=abs(second.getY()-first.getY())+1;
            int i=first.getY();
            int dir;
            if (second.getY()-first.getY()>=0) dir=1;
            else dir=-1;

            while (i!=second.getY()){
                positions.add(new Coordinate(first.getX(),i));
                i=i+dir;



            }
            positions.add(new Coordinate(second));


        }else{
            int compare="RESPUBLIKA".indexOf(second.getX())-"RESPUBLIKA".indexOf(first.getX());
            length = abs(compare)+1;
            int i="RESPUBLIKA".indexOf(first.getX());
            int dir=(compare>=0)?1:-1;

            while (i!="RESPUBLIKA".indexOf(second.getX())){
                positions.add(new Coordinate("RESPUBLIKA".charAt(i),first.getY()));
                i=i+dir;
            }
            positions.add(new Coordinate(second));
        }

    }


    public Ship(Coordinate hit, boolean destroyed){
        this.positions.add(hit);
        if(destroyed){
            this.length=1;
            this.destroyed=true;
        }
    }

    public void hit(Coordinate hit, boolean destroyed){
        this.positions.add(hit);
        if(destroyed){
            this.length=1;
            this.destroyed=true;
        }

    }

    public int getLength() {
        return length;
    }

    public ArrayList<Coordinate> getPositions() {
        return positions;
    }

    public SeaOrientation getOrientation() {
        return orientation;
    }

    public void hit(){
        hitnumber++;
        if (hitnumber==length) destroyed=true;
    }

    public boolean isDestroyed(){
        return destroyed;
    }
}
